import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

public class Supermarket extends JFrame {

	// Objects

	MouseHandler mouseHandler = new MouseHandler();
	ActionHandler actionHandler = new ActionHandler();
	FocusHandler focusHandler = new FocusHandler();
	GridBagConstraints gbc = new GridBagConstraints();
	GridBagConstraints gbcadd = new GridBagConstraints();
	GridBagConstraints gbcedit = new GridBagConstraints();

	// Variables

	int pX = 0, pY = 0;
	String[] catagories = { "Food and Beverages", "Electronics", "Clothings", "Other" };

	// Images

	ImageIcon icon = new ImageIcon("C:\\Users\\aresama\\WorkSpace\\Supermarket Management System\\1x\\Asset 7.png");
	ImageIcon minIcon = new ImageIcon("C:\\Users\\aresama\\WorkSpace\\Supermarket Management System\\1x\\Asset 8.png");
	ImageIcon maxIcon = new ImageIcon("C:\\Users\\aresama\\WorkSpace\\Supermarket Management System\\1x\\Asset 9.png");
	ImageIcon closeIcon = new ImageIcon(
			"C:\\Users\\aresama\\WorkSpace\\Supermarket Management System\\1x\\Asset 10.png");
	ImageIcon logoIcon = new ImageIcon("C:\\Users\\aresama\\WorkSpace\\Supermarket Management System\\1x\\Asset 1.png");
	ImageIcon homeIcon = new ImageIcon("C:\\Users\\aresama\\WorkSpace\\Supermarket Management System\\1x\\Asset 6.png");
	ImageIcon productsIcon = new ImageIcon(
			"C:\\Users\\aresama\\WorkSpace\\Supermarket Management System\\1x\\Asset 5.png");
	ImageIcon onlineIcon = new ImageIcon(
			"C:\\Users\\aresama\\WorkSpace\\Supermarket Management System\\1x\\Asset 2.png");
	ImageIcon reportIcon = new ImageIcon(
			"C:\\Users\\aresama\\WorkSpace\\Supermarket Management System\\1x\\Asset 3.png");
	ImageIcon aboutIcon = new ImageIcon(
			"C:\\Users\\aresama\\WorkSpace\\Supermarket Management System\\1x\\Asset 4.png");

	// Colors and Fonts

	Font font = new Font("Montserrat", Font.BOLD, 12);
	Font fontPlain = new Font("Montserrat", Font.PLAIN, 12);
	Color blue = new Color(204, 251, 246);
	Color white = new Color(255, 255, 255);
	Color fade = new Color(235, 253, 251);

	// Panels

	JPanel mainPanel = new JPanel(new BorderLayout());
	JPanel head = new JPanel(new BorderLayout());
	JPanel headTools = new JPanel(new GridLayout());

	JPanel sidePanel = new JPanel(new BorderLayout());
	JPanel logoPanel = new JPanel(new BorderLayout());
	JPanel menuPanel = new JPanel(new GridLayout(6, 1, 10, 10));
	JPanel homeButtonPanel = new JPanel(new GridLayout(1, 3, 2, 2));
	JPanel productsButtonPanel = new JPanel(new GridLayout(1, 3, 2, 2));
	JPanel onlineButtonPanel = new JPanel(new GridLayout(1, 3, 2, 2));
	JPanel reportButtonPanel = new JPanel(new GridLayout(1, 3, 2, 2));
	JPanel aboutButtonPanel = new JPanel(new GridLayout(1, 3, 2, 2));

	JPanel centerPanel = new JPanel(new GridBagLayout());

	JPanel homePanel = new JPanel(new BorderLayout());
	JTabbedPane homeTab = new JTabbedPane();
	JPanel trending = new JPanel(new BorderLayout());

	JPanel productsPanel = new JPanel(new BorderLayout());
	JTabbedPane productsTab = new JTabbedPane();

	JPanel addPanel = new JPanel();
	GridBagLayout gb = new GridBagLayout();

	JPanel editPanel = new JPanel(new GridBagLayout());
	JPanel editSearchPanel = new JPanel(new BorderLayout());
	JPanel editSearchTf = new JPanel(new BorderLayout());
	JPanel editTable = new JPanel(new GridBagLayout());

	JPanel deletePanel = new JPanel(new GridLayout());
	JPanel viewPanel = new JPanel(new GridLayout());

	JPanel onlinePanel = new JPanel(new BorderLayout());
	JTabbedPane onlineTab = new JTabbedPane();
	JPanel onlineP = new JPanel();

	JPanel reportPanel = new JPanel(new BorderLayout());
	JTabbedPane reportTab = new JTabbedPane();
	JPanel reportP = new JPanel();

	JPanel aboutPanel = new JPanel(new BorderLayout());
	JTabbedPane aboutTab = new JTabbedPane();
	JPanel aboutP = new JPanel();

	// Labels

	JLabel programIcon = new JLabel(icon);
	JLabel logo = new JLabel(logoIcon);
	JLabel homeIconLabel = new JLabel(homeIcon);
	JLabel productsIconLabel = new JLabel(productsIcon);
	JLabel onlineIconLabel = new JLabel(onlineIcon);
	JLabel reportIconLabel = new JLabel(reportIcon);
	JLabel aboutIconLabel = new JLabel(aboutIcon);
	JLabel nameOfTheProduct = new JLabel("Name Of The Product");
	JLabel catagory = new JLabel("Catagory");
	JLabel priceOfTheProduct = new JLabel("Price Of The Product");
	JLabel quantity = new JLabel("Quantity");
	JLabel discription = new JLabel("Discription");
	JLabel priceValidation = new JLabel();
	JLabel quantityValidation = new JLabel();

	// JTextFields

	JTextField nameTf = new JTextField("Enter The Name Of The Product", 30);
	JTextField priceTf = new JTextField("Enter The Price", 30);
	JTextField quantityTf = new JTextField("Enter The Quantity", 30);
	JTextField editTf = new JTextField("Search");

	// JComboBox
	JComboBox catagoryBox = new JComboBox(catagories);
	JTextArea discriptionArea = new JTextArea(3, 30);

	// Buttons

	JButton addButton = new JButton("ADD");
	JButton min = new JButton(minIcon);
	JButton max = new JButton(maxIcon);
	JButton close = new JButton(closeIcon);
	JButton home = new JButton("Home");
	JButton products = new JButton("Products");
	JButton online = new JButton("Online Store");
	JButton report = new JButton("Report");
	JButton about = new JButton("About");

	// Table

	JTable table = new JTable();
	DefaultTableModel tableModel = new DefaultTableModel(new Object[0][5],
			new String[] { "Name", "Catagory", "Price", "Quantity", "Discription" });

	// Methods

	private void maximize() {
		// Get GraphicsEnvironment object for getting GraphicsDevice object
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		// Get the screen devices
		GraphicsDevice[] g = env.getScreenDevices();
		// I only have one, the first one
		// If current window is full screen, set fullscreen window to null
		// else set the current screen
		g[0].setFullScreenWindow(g[0].getFullScreenWindow() == this ? null : this);
	}

	private class MouseHandler implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mouseEntered(MouseEvent event) {
			if (event.getComponent() == homeButtonPanel || event.getSource() == home) {
				homeButtonPanel.setBackground(fade);
			} else if (event.getComponent() == productsButtonPanel || event.getSource() == products) {
				productsButtonPanel.setBackground(fade);
			} else if (event.getComponent() == onlineButtonPanel || event.getSource() == online) {
				onlineButtonPanel.setBackground(fade);
			} else if (event.getComponent() == reportButtonPanel || event.getSource() == report) {
				reportButtonPanel.setBackground(fade);
			} else if (event.getComponent() == aboutButtonPanel || event.getSource() == about) {
				aboutButtonPanel.setBackground(fade);
			}
		}

		@Override
		public void mouseExited(MouseEvent event) {
			homeButtonPanel.setBackground(blue);
			productsButtonPanel.setBackground(blue);
			onlineButtonPanel.setBackground(blue);
			reportButtonPanel.setBackground(blue);
			aboutButtonPanel.setBackground(blue);

		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

	}

	public class ActionHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent ae) {
			if (ae.getSource() == home) {
				homePanel.setVisible(true);
				productsPanel.setVisible(false);
				onlinePanel.setVisible(false);
				reportPanel.setVisible(false);
				aboutPanel.setVisible(false);
				if (homePanel.isVisible()) {
					homeButtonPanel.setBackground(white);
				}
			} else if (ae.getSource() == products) {
				homePanel.setVisible(false);
				productsPanel.setVisible(true);
				onlinePanel.setVisible(false);
				reportPanel.setVisible(false);
				aboutPanel.setVisible(false);
				if (productsPanel.isVisible()) {
					productsButtonPanel.setBackground(white);
				}
			} else if (ae.getSource() == online) {
				homePanel.setVisible(false);
				productsPanel.setVisible(false);
				onlinePanel.setVisible(true);
				reportPanel.setVisible(false);
				aboutPanel.setVisible(false);
				if (onlinePanel.isVisible()) {
					onlineButtonPanel.setBackground(white);
				}
				try {
					File html = new File("C:\\Users\\aresama\\Desktop\\aastu\\index.html");
					Desktop.getDesktop().browse(html.toURI());
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (ae.getSource() == report) {
				homePanel.setVisible(false);
				productsPanel.setVisible(false);
				onlinePanel.setVisible(false);
				reportPanel.setVisible(true);
				aboutPanel.setVisible(false);
				if (reportPanel.isVisible()) {
					reportButtonPanel.setBackground(white);
				}
			} else if (ae.getSource() == about) {
				homePanel.setVisible(false);
				productsPanel.setVisible(false);
				onlinePanel.setVisible(false);
				reportPanel.setVisible(false);
				aboutPanel.setVisible(true);
				if (aboutPanel.isVisible()) {
					aboutButtonPanel.setBackground(white);
				}
			}
		}
	}

	private class FocusHandler implements FocusListener {
		@Override
		public void focusGained(FocusEvent fo) {
			fo.getComponent().setForeground(Color.black);
			if (fo.getSource() == nameTf) {
				if (nameTf.getText().equals("Enter The Name Of The Product")) {
					nameTf.setText("");
				}
			} else if (fo.getSource() == priceTf) {
				if (priceTf.getText().equals("Enter The Price")) {
					priceTf.setText("");
				}
			} else if (fo.getSource() == quantityTf) {
				if (quantityTf.getText().equals("Enter The Quantity")) {
					quantityTf.setText("");
				}
			} else if (fo.getSource() == editTf) {
				if (editTf.getText().equals("Search")) {
					editTf.setText("");
				}
			}
		}

		@Override
		public void focusLost(FocusEvent fe) {
			fe.getComponent().setForeground(Color.LIGHT_GRAY);
			if (fe.getSource() == nameTf) {
				nameTf.setText("Enter The Name Of The Product");
			} else if (fe.getSource() == priceTf) {
				priceTf.setText("Enter The Price");
			} else if (fe.getSource() == quantityTf) {
				quantityTf.setText("Enter The Quantity");
			} else if (fe.getSource() == editTf) {
				editTf.setText("Search");
			}
		}
	}

	private void addComponent(Component component, int x, int y) {

		gbcadd.insets = new Insets(20, 0, 5, 5);
		gbcadd.gridx = x;
		gbcadd.gridy = y;
		gbcadd.weightx = 1;
		gbcadd.fill = GridBagConstraints.NONE;
		gbcadd.anchor = GridBagConstraints.NORTHWEST;
		if (component == discriptionArea) {
			addPanel.add(new JScrollPane(discriptionArea), gbcadd);

		} else {
			addPanel.setLayout(gb);
			addPanel.add(component, gbcadd);
		}
	}

	public Supermarket() {

		setSize(1000, 530);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setUndecorated(true);
		setLocationRelativeTo(null);

		// Frame look and Feel

		UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
		try {
			UIManager.setLookAndFeel(looks[1].getClassName());
			SwingUtilities.updateComponentTreeUI(homeTab);
			SwingUtilities.updateComponentTreeUI(productsTab);
			SwingUtilities.updateComponentTreeUI(onlineTab);
			SwingUtilities.updateComponentTreeUI(reportTab);
			SwingUtilities.updateComponentTreeUI(aboutTab);
			SwingUtilities.updateComponentTreeUI(addPanel);
			SwingUtilities.updateComponentTreeUI(nameTf);
			SwingUtilities.updateComponentTreeUI(priceTf);
			SwingUtilities.updateComponentTreeUI(catagoryBox);
			SwingUtilities.updateComponentTreeUI(quantityTf);
			SwingUtilities.updateComponentTreeUI(discriptionArea);
			SwingUtilities.updateComponentTreeUI(addButton);
			SwingUtilities.updateComponentTreeUI(editTable);
			SwingUtilities.updateComponentTreeUI(editTf);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		setIconImage(icon.getImage());

		// headTools

		headTools.add(min);
		headTools.add(max);
		headTools.add(close);
		headTools.setBackground(blue);

		min.setFocusPainted(false);
		min.setContentAreaFilled(false);
		min.setBorderPainted(false);
		max.setFocusPainted(false);
		max.setContentAreaFilled(false);
		max.setBorderPainted(false);
		close.setFocusPainted(false);
		close.setContentAreaFilled(false);
		close.setBorderPainted(false);

		// head

		head.add(programIcon, BorderLayout.WEST);
		head.add(headTools, BorderLayout.EAST);
		head.setBackground(blue);

		// head ActionListener

		min.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				// minimize
				setState(ICONIFIED);
			}
		});
		max.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				maximize();
			}
		});
		close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				// terminate program
				System.exit(0);
			}
		});
		head.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				// Get x,y and store them
				pX = me.getX();
				pY = me.getY();
			}
		});
		// Add MouseMotionListener for detecting drag
		head.addMouseMotionListener(new MouseAdapter() {
			public void mouseDragged(MouseEvent me) {
				// Set the location
				// get the current location x-co-ordinate and then get
				// the current drag x co-ordinate, add them and subtract most recent
				// mouse pressed x co-ordinate
				// do same for y co-ordinate
				setLocation(getLocation().x + me.getX() - pX, getLocation().y + me.getY() - pY);
			}
		});

		// menuPanel

		homeButtonPanel.setBackground(blue);
		homeButtonPanel.add(homeIconLabel);
		homeButtonPanel.add(home);
		home.setFont(font);
		home.setFocusPainted(false);
		home.setBorderPainted(false);
		home.setContentAreaFilled(false);
		home.setHorizontalAlignment(SwingConstants.LEFT);
		homeButtonPanel.addMouseListener(mouseHandler);
		home.addMouseListener(mouseHandler);
		home.addActionListener(actionHandler);

		productsButtonPanel.setBackground(blue);
		productsButtonPanel.add(productsIconLabel);
		productsButtonPanel.add(products);
		products.setFont(font);
		products.setFocusPainted(false);
		products.setBorderPainted(false);
		products.setContentAreaFilled(false);
		products.setHorizontalAlignment(SwingConstants.LEFT);
		productsButtonPanel.addMouseListener(mouseHandler);
		products.addMouseListener(mouseHandler);
		products.addActionListener(actionHandler);

		onlineButtonPanel.setBackground(blue);
		onlineButtonPanel.add(onlineIconLabel);
		online.setFont(font);
		onlineButtonPanel.add(online);
		online.setFocusPainted(false);
		online.setBorderPainted(false);
		online.setContentAreaFilled(false);
		online.setHorizontalAlignment(SwingConstants.LEFT);
		onlineButtonPanel.addMouseListener(mouseHandler);
		online.addMouseListener(mouseHandler);
		online.addActionListener(actionHandler);

		reportButtonPanel.setBackground(blue);
		reportButtonPanel.add(reportIconLabel);
		reportButtonPanel.add(report);
		report.setFont(font);
		report.setFocusPainted(false);
		report.setBorderPainted(false);
		report.setContentAreaFilled(false);
		report.setHorizontalAlignment(SwingConstants.LEFT);
		reportButtonPanel.addMouseListener(mouseHandler);
		report.addMouseListener(mouseHandler);
		report.addActionListener(actionHandler);

		aboutButtonPanel.setBackground(blue);
		aboutButtonPanel.add(aboutIconLabel);
		aboutButtonPanel.add(about);
		about.setFont(font);
		about.setFocusPainted(false);
		about.setBorderPainted(false);
		about.setContentAreaFilled(false);
		about.setHorizontalAlignment(SwingConstants.LEFT);
		aboutButtonPanel.addMouseListener(mouseHandler);
		about.addMouseListener(mouseHandler);
		about.addActionListener(actionHandler);

		menuPanel.setBackground(blue);
		menuPanel.add(homeButtonPanel);
		menuPanel.add(productsButtonPanel);
		menuPanel.add(onlineButtonPanel);
		menuPanel.add(reportButtonPanel);
		menuPanel.add(aboutButtonPanel);

		// sidePanel

		sidePanel.setBackground(blue);
		sidePanel.setPreferredSize(new Dimension(250, 500));
		logoPanel.setPreferredSize(new Dimension(250, 130));
		logoPanel.setBackground(blue);
		logoPanel.add(logo);
		sidePanel.add(logoPanel, BorderLayout.NORTH);
		sidePanel.add(menuPanel, BorderLayout.CENTER);

		// centerPanel

		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.weightx = 250;
		gbc.weighty = 500;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.ipadx = 750;
		gbc.ipady = 500;
		centerPanel.setBackground(white);

		// homePanel

		homePanel.setBackground(white);
		homeTab.setFont(fontPlain);
		trending.setBackground(white);
		homePanel.setFont(fontPlain);
		homeTab.addTab("Trending", null, trending, "Trending");
		homePanel.add(homeTab);
		JLabel welcome = new JLabel("WELCOME");
		JLabel bestSellingItems = new JLabel("Best Selling Items");
		welcome.setFont(new Font("Montserrat", Font.BOLD, 20));
		welcome.setForeground(Color.orange);
		bestSellingItems.setFont(font);
		trending.add(welcome, BorderLayout.NORTH);
		trending.add(bestSellingItems, BorderLayout.LINE_START);
		homePanel.setVisible(true);
		centerPanel.add(homePanel, gbc);

		// Products Panel

		addPanel.setBackground(white);
		addPanel.setFont(font);
		editPanel.setBackground(white);
		deletePanel.setBackground(white);
		viewPanel.setBackground(white);

		productsTab.setFont(fontPlain);
		productsTab.addTab("Add Products", null, addPanel, "Add Products");
		productsTab.addTab("Edit Products", null, editPanel, "Edit Products");
		productsTab.addTab("Delete Products", null, deletePanel, "Delete Products");
		productsTab.addTab("View Products", null, viewPanel, "View Products");
		productsPanel.add(productsTab);
		productsPanel.setVisible(false);
		centerPanel.add(productsPanel, gbc);

		// addPanel

		nameOfTheProduct.setFont(font);
		catagory.setFont(font);
		priceOfTheProduct.setFont(font);
		quantity.setFont(font);
		discription.setFont(font);
		priceValidation.setFont(fontPlain);
		priceValidation.setForeground(Color.RED);
		quantityValidation.setFont(fontPlain);
		quantityValidation.setForeground(Color.RED);

		nameTf.setForeground(Color.LIGHT_GRAY);
		priceTf.setForeground(Color.LIGHT_GRAY);
		quantityTf.setForeground(Color.LIGHT_GRAY);
		nameTf.addFocusListener(focusHandler);
		priceTf.addFocusListener(focusHandler);
		quantityTf.addFocusListener(focusHandler);

		addComponent(nameOfTheProduct, 1, 1);
		addComponent(nameTf, 2, 1);
		addComponent(catagory, 1, 2);
		addComponent(catagoryBox, 2, 2);
		addComponent(priceOfTheProduct, 1, 3);
		addComponent(priceTf, 2, 3);
		addComponent(priceValidation, 3, 3);
		addComponent(quantity, 1, 4);
		addComponent(quantityTf, 2, 4);
		addComponent(quantityValidation, 3, 4);
		addComponent(discription, 1, 5);
		addComponent(discriptionArea, 2, 5);
		addComponent(addButton, 3, 7);

		// addPanel textField Listeners

		priceTf.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(20);
						} catch (InterruptedException ex) {
						}
						char key = e.getKeyChar();
						String price = priceTf.getText();
						for (int i = 0; i < price.length(); i++) {
							if (!(Character.isISOControl(price.charAt(i)) || Character.isDigit(price.charAt(i))
									|| price.charAt(i) == '.')) {
								priceValidation.setText("Invalid Entry!");
								return;
							}
						}
						priceValidation.setText("");
					}
				});
			}

			@Override
			public void keyPressed(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {

			}
		});

		quantityTf.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(20);
						} catch (InterruptedException ex) {
						}
						char key = e.getKeyChar();
						String price = quantityTf.getText();
						for (int i = 0; i < price.length(); i++) {
							if (!(Character.isISOControl(price.charAt(i)) || Character.isDigit(price.charAt(i))
									|| price.charAt(i) == '.')) {
								quantityValidation.setText("Invalid Entry!");
								return;
							}
						}
						quantityValidation.setText("");
					}
				});
			}

			@Override
			public void keyPressed(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {

			}
		});

		// editPanel

		gbcedit.fill = GridBagConstraints.BOTH;
		gbcedit.weightx = 1;
		gbcedit.weighty = 1;

		editTf.setForeground(Color.LIGHT_GRAY);
		editTf.addFocusListener(focusHandler);
		table.setModel(tableModel);
		table.setAutoCreateRowSorter(true);
		table.setBackground(white);
		editSearchPanel.setBackground(white);
		editTable.setBackground(white);
		editTable.add(new JScrollPane(table), gbcedit);
		editSearchTf.add(editTf, BorderLayout.CENTER);
		editSearchPanel.add(editSearchTf, BorderLayout.NORTH);
		editSearchPanel.add(editTable);
		editSearchPanel.setMinimumSize(new Dimension(750, 500));
		editPanel.add(editSearchPanel, gbc);

		// OnlinePanel

		onlinePanel.setBackground(white);
		onlineP.setBackground(white);
		onlineTab.addTab("Online Store", null, onlineP, "Online Store");
		onlinePanel.add(onlineTab);
		onlinePanel.setVisible(false);
		onlineP.add(new JLabel("Connecting...."), BorderLayout.CENTER);
		centerPanel.add(onlinePanel, gbc);

		// reportPanel

		reportP.setBackground(white);
		reportTab.addTab("Report", null, reportP, "report");
		reportPanel.add(reportTab);
		reportPanel.setVisible(false);
		centerPanel.add(reportPanel, gbc);

		// aboutPanel

		aboutP.setBackground(white);
		aboutTab.addTab("About Us", null, aboutP, "About Us");
		aboutPanel.add(aboutTab);
		aboutPanel.setVisible(false);
		JLabel about = new JLabel("Java Group Work \n\nI Think it Works");
		about.setFont(font);
		aboutP.add(about);
		centerPanel.add(aboutPanel, gbc);

		// mainPanel

		mainPanel.add(centerPanel, BorderLayout.CENTER);
		mainPanel.add(sidePanel, BorderLayout.WEST);

		// AddToTheMainFrame

		add(head, BorderLayout.NORTH);
		add(mainPanel, BorderLayout.CENTER);
	}

}

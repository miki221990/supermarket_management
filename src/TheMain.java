import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class TheMain {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
//				Supermarket superMarket = new Supermarket();
//				superMarket.setSize(1000, 530);
//				superMarket.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//				superMarket.setUndecorated(true);
//				superMarket.setLocationRelativeTo(null);
//				superMarket.setVisible(true);
				SignIn signIn = new SignIn();
				signIn.setSize(750,400);
				signIn.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				signIn.setLocationRelativeTo(null);
				signIn.setVisible(true);
			}
		});
	}
}
